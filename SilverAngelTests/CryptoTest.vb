﻿Imports System.Text
Imports SilverAngel
Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> Public Class CryptoTest

    <TestMethod()> Public Sub SecurePasswordTest()
        'Generate 100 passwords and all should be unique
        Dim passwordList As New List(Of String)
        For i = 0 To 99
            passwordList.Add(GenerateSecurePassword())
        Next i

        ' Sort list for fast searching
        passwordList.Sort()

        Dim lastPassword = String.Empty
        For Each password As String In passwordList
            Assert.AreNotEqual(password, lastPassword)
            lastPassword = password
        Next
    End Sub

    <TestMethod()> Public Sub TestMD5()
        Dim catsMd5 = GetMD5("cats")
        Const catsPrecomputedResult As String = "0832c1202da8d382318e329a7c133ea0"

        Assert.AreEqual(catsMd5, catsPrecomputedResult)
    End Sub

End Class
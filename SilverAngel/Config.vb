﻿
''' <summary>
'''     Contains various settings for SilverAngel
''' </summary>
''' <remarks>Placeholder module until config loading is implemented</remarks>
Public Module Config
    Public Class Network
        Public Shared Server = "chat.freenode.net"
        Public Shared Port = 6667
    End Class

    Public Class Authentication
        Public Shared SecretPassword = "SilverAngel"
    End Class

    Public Class Core
        Public Shared PacketSize = 2048 '2KB
    End Class
End Module

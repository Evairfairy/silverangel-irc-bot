﻿Imports System.Text
Imports System.Security.Cryptography

Public Module Cryptography
    ' Arbitrary seeding ftw
    Public RNG As New Random((DateTime.Now.Millisecond + 682)*(DateTime.Now.Minute + 106) + DateTime.Now.Second*173)

    Public Function GenerateSecurePassword()
        Dim sb As New StringBuilder
        sb.Append(RNG.NextDouble())
        sb.Append(Guid.NewGuid())
        sb.Append(RNG.NextDouble())
        sb.Append(Authentication.SecretPassword)
        sb.Append(RNG.NextDouble())
        Dim returnVal = GetMD5(sb.ToString())
        For i = 0 To 100
            returnVal = GetMD5(returnVal)
        Next i
        Return returnVal
    End Function

    Public Function GetMD5(ByVal plaintext As String)
        Using md5HashFactory = MD5.Create
            Dim data = md5HashFactory.ComputeHash(Encoding.UTF8.GetBytes(plaintext))
            Dim sb = New StringBuilder

            For i = 0 To data.Length - 1
                sb.Append(data(i).ToString("x2"))
            Next i

            Return sb.ToString
        End Using
    End Function
End Module

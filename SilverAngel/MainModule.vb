﻿Imports System.Net.Sockets
Imports System.Text
Imports System.Threading

Module MainModule

    Sub Main()
        Dim connection = New IRCConnection(Network.Server, Network.Port)
        connection.Connect()
        While connection.IsConnected
            Thread.Sleep(1000)
        End While

        Console.WriteLine("End of MainModule")
        Console.ReadLine()
    End Sub

End Module

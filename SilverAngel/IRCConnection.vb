﻿Imports System.Net.Sockets
Imports System.Threading
Imports System.Text

Public Class IRCConnection
    Dim _client As TcpClient
    Dim _ns As NetworkStream
    Dim _host As String
    Dim _port As Integer
    Dim _connected As Boolean
    Dim _threadList As New List(Of Thread)

    Public Property IsConnected
        Get
            Return _connected
        End Get
        Set(value)
            _connected = value
        End Set
    End Property
    Public Property Client
        Get
            Return _client
        End Get
        Set(value)
            _client = value
        End Set
    End Property

    Public Property Stream
        Get
            Return _ns
        End Get
        Set(value)
            _ns = value
        End Set
    End Property

    Public Property Host
        Get
            Return _host
        End Get
        Set(value)
            _host = value
        End Set
    End Property

    Public Property Port
        Get
            Return _port
        End Get
        Set(value)
            _port = value
        End Set
    End Property

    Public Sub Connect()
        Try
            Client = New TcpClient(Host, Port)
            Stream = _client.GetStream()
            IsConnected = True
            Dim thread As New Thread(AddressOf MainDataThread)
            thread.IsBackground = True
            thread.Start()
            _threadList.Add(thread)
            Dim monitor As New Thread(AddressOf MonitorThread)
            monitor.IsBackground = True
            monitor.Start()
        Catch ex As Exception
            LogLine("Exception occurred:\n {0}", ex)
        End Try
    End Sub
    Public Function SplitCommands(ByVal data As String) As List(Of IRCCommand)
        Dim commands As New List(Of String)
        If data.Contains(vbNewLine) Then
            commands.AddRange(From cmd In data.Split(vbNewLine).ToList() Where IRCCommand.IsIrcCommand(cmd))
        Else
            commands.Add(data)
        End If
        'Wow, not even half a day into this project and the spaghetti code has already begun
        For i = 0 To commands.Count - 1
            commands(i) = commands(i).Trim()
        Next i
        Dim ircCommands As List(Of IRCCommand) = (From command In commands Select New IRCCommand(command)).ToList()
        Return ircCommands
    End Function
    Public Sub MainDataThread()
        Dim ns = Stream
        While IsConnected
            Dim buffer(Core.PacketSize) As Byte
            Dim bytesRead = Stream.Read(buffer, 0, Core.PacketSize)
            If bytesRead.Equals(0) Then
                IsConnected = False
                Exit While
            End If

            Array.Resize(buffer, bytesRead)
            'Console.WriteLine(Encoding.UTF8.GetString(buffer))
            Dim cmdString As String = Encoding.UTF8.GetString(buffer)

            Dim i = 0
            For Each cmd As IRCCommand In SplitCommands(cmdString)
                i = i + 1
                If cmd.Prefix.Equals(String.Empty) Then
                    Continue For
                End If
                Console.WriteLine("--- BEGIN IRC COMMAND ---")
                'Console.WriteLine("Raw data: [{0}]", cmdString)
                Console.WriteLine("Command ID: [{0}]", i)
                Console.WriteLine("Prefix: [{0}]", cmd.Prefix)
                Console.WriteLine("Command: [{0}]", cmd.Command)
                Console.WriteLine("Params: [{0}]", cmd.Params)
                Console.WriteLine("Trailing Data: [{0}]", cmd.TrailingData)
                Console.WriteLine("--- END IRC COMMAND ---")
                Console.WriteLine()
            Next
        End While
        Console.WriteLine("Disconnected from server")
    End Sub

    Public Sub MonitorThread()
        While IsConnected
            Thread.Sleep(1000)
        End While

        ' Shutting down, destroy all threads
        For Each thread As Thread In _threadList
            thread.Abort()
        Next
    End Sub

    Public Sub New(ByVal host As String, ByVal port As Integer)
        Me.Host = host
        Me.Port = port
    End Sub
End Class

﻿'TODO: Data Validation; right now, a server could crash us by sending a bad command
'TODO: Replace all the parsing code with regex
Public Class IRCCommand
    Public Prefix As String
    Public Command As String
    Public Params As String
    Public TrailingData As String
    Public Sub ExtractCommand(ByRef data As String)
        Dim endCommand = data.IndexOf(" ", StringComparison.Ordinal)
        Command = data.Substring(0, endCommand)
        'Strip command from string
        data = data.Substring(endCommand + 1)
    End Sub
    Public Sub ExtractTrailingData(ByRef data As String)
        If Not data.Contains(":") Then
            TrailingData = String.Empty
            Return
        End If
        Dim beginTrailingData = data.IndexOf(":", StringComparison.Ordinal)
        TrailingData = data.Substring(beginTrailingData)
        'Strip trailing data from string
        data = data.Substring(0, beginTrailingData - 2)
    End Sub
    Public Sub ExtractPrefix(ByRef data As String)
        If Not data.StartsWith(":") Then
            Prefix = String.Empty
            Return
        End If
        Dim endOfPrefix = data.IndexOf(" ", StringComparison.Ordinal)
        Prefix = data.Substring(1, endOfPrefix - 1)
        ' Strip rest of string, we don't need it
        data = data.Substring(endOfPrefix + 1)
    End Sub
    Public Sub ParseCommand(ByVal data As String)
        ExtractPrefix(data)
        ExtractTrailingData(data)
        ExtractCommand(data)
        Params = data
    End Sub
    Public Sub New(ByVal data As String)
        'LogLine("Parsing: {0}", data)
        data = data.Replace(vbNewLine, "")
        data = data.Replace(vbCr, "")
        data = data.Replace(vbCrLf, "")
        Try
            ParseCommand(data)
        Catch ex As Exception
            Logging.LogLine("Exception occurred parsing IRC command: {0}{1}", vbNewLine, ex)
            Prefix = String.Empty
            Command = String.Empty
            Params = String.Empty
            TrailingData = String.Empty
        End Try
    End Sub
    Public Shared Function IsIrcCommand(ByVal data As String)
        'TODO: add regex
        If data.Length > 3 Then
            Return True
        End If
        Return False
    End Function
End Class
